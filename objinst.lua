require "quadani"
require "meshani"
require "widgets"

-- o.gfx -- definition of a quadani/meshani, etc...
-- o.gfxo -- instance parameters (quad, mesh, etc..)
objinst={
-- certains paramètres peuvent être dans l'animation
-- puis écrasé par l'instance et réciproquement?
__call=function(o, struct)
  local o={} setmetatable(o,objinst)
  -- print("instantiate "..struct.name)
  o.struct = struct
  if debug then print("avant if "..struct.name..".gfx.struct="..struct.gfx_struct.name) end
  if struct.gfx_struct ~= nil then local g=struct.gfx_struct
    if debug then print(struct.name..".gfx.struct="..g.name) end
    if g.inst == nil then -- si pas encore d'instance de cette animation, en crée une
      if debug then print("create inst of "..g.name) end
      if g.type == "meshani" then
        if debug then
          print(g.name.." => meshani")
          print(struct.name..": o.gfx = meshani(g)")
        end
        o.gfx = meshani(g)
        o.gfxo = o.gfx:data_inst() --own instance
        if debug then
          if o.gfxo == nil then
            print(struct.name.." o.gfxo est nil")
          else
            print(struct.name.." o.gfxo n'est pas nil")
          end
        end
      elseif g.type == "quadani" then
        if debug then 
          print(g.name.." => quadani")
          print(struct.name..": o.gfx=g.quadani(g)")
        end
        o.gfx = quadani(g)
        o.gfxo = o.gfx:data_inst()
      else
        print(g.mname.."=> "..g.type.." unknown type")
      end
    else
      if debug then print(struct.name..": o.gfx=g.inst = "..g.inst.imgname) end
      -- une entité graphie existe déjà, réutilise
      o.gfx = g.inst
      o.gfxo = o.gfx:data_inst()
    end
  else
--    print(struct.name.."struct.gfx_struct est nul")
  end
 
  o:set_anim(struct.animi)

 -- dynamique
  if struct.p ~= nil then -- position
    o.p = {x=struct.p.x, y=struct.p.y}
  else o.p = {x=0,y=0} end
  if struct.v ~= nil then -- vitesse
    o.v = {x=struct.v.x, y=struct.v.y}
  else o.v = {x=0,y=0} end
  if struct.a ~= nil then -- acceleration
    o.a = {x=struct.a.x, y=struct.a.y}
  else o.a = {x=0,y=0} end

  if struct.vmax ~= nil then -- vitesse
    o.vmax = struct.vmax
  else o.vmax = 3 end  
  if struct.acc ~= nil then -- vitesse
    o.acc = struct.acc
  else o.acc = 0.05 end
  if struct.decel ~= nil then -- vitesse
    o.decel = struct.decel
  else o.decel = 0.25 end  
  
  -- rotscale
  if struct.rot ~= nil then
    o.rot = struct.rot
  else o.rot = 0 end
    if struct.scale ~= nil then
    o.scale = {x=struct.scale.x, y=struct.scale.y}
  else o.scale = {x=1,y=1} end
    
  o.childs = {}
  if struct.childs ~= nil then
    if debug then print(o.struct.name..": struct.childs != nil") end
    for i = 1, #struct.childs do
      table.insert(o.childs, obj_load(struct.childs[i]))
    end
  end
  
  return o
end,

add_gfx=function(o,gfx)
  o.gfx=gfx
  if o.gfx.classname == "meshani" or o.gfx.classname == "quadani" then
    o.gfxo = o.gfx:data_inst()
  end
end,

-- redondant ?
reset_anim=function(o)
  set_anim(o,o.struct.animi) --
end,

set_anim=function(o, ani)
  o.ftime = love.timer.getTime()
 -- recupère depuis la structure ?
  if ani == nil then
    o.startanim = 0
    o.frame = 1
    o.animfinish = false
    o.fps = 16
    o.innerloop = nil
  -- o.innerloop={s
    o.forward = false
    o.func = nil
    o:set_frameupdate(nil)
  else
    if ani.startanim ~= nil then
      o.startanim = ani.startanim
    else o.startanim = 0 end
    if ani.forward ~= nil then
      o.forward = ani.forward
    else o.forward = true end
    if ani.frame ~= nil then
      o.frame = ani.frame
    else o.frame = 1 end
    o.animfinish = false
    if ani.fps ~= nil then
      o.fps = ani.fps
    else o.pfs = 12 end
    if ani.innerloop ~= nil then
      o.innerloop = ani.innerloop
    else o.innerloop = nil end
  -- o.innerloop={s
    o:set_frameupdate(ani.func)
  end
end,

touchCenter=function(o,p,R)
  if R == nil then R = 1 end
  -- TODO: mettre 1 ou == 0 ? définir une variable dans l'instance ? -- match le centre
  return (o.p.x - p.x)^2 + (o.p.y - p.y)^2 <= R*R
end,

touch=function(o,p,R)
  if R == nil then R = 1 end
  local mx,my = o.p.x-o.gfx.ctr.x, o.p.y-o.gfx.ctr.y
  return p.x+R > mx and p.x-R < mx+o.gfx.acell.w and
         p.y+R > my and p.y-R < my+o.gfx.acell.h
end,

-- R peut être un rayon X, un rayo vectoriel X/Y ou nul
isIn=function(o,p,R)
  -- poibnt up left and down rgght
  local UL = { x = o.p.x - o.gfxo.ctr.x, y = o.p.x - o.gfxo.ctr.y }
  local DR = { x = o.p.x - o.gfxo.ctr.x + o.gfx.acell.w, y = o.p.x - o.gfxo.ctr.y + o.gfx.acell.h  }

  if R == nil then
    -- TODO: mettre 1 ou == 0 ? définir une variable dans l'instance ? -- match le centre
    if UL.x <= p.x and DR.x >= p.x and UL.y <= p.y and DR.y >= p.y then
     return true
    end
  elseif type(R) == 'number' then -- R scalaire
    return false
  else -- R vecteur
    return false
  end
  return false
end,

start_anim=function(o,time)
 -- print(o.gfx.imgname..": startanim")
  if time == nil then time = love.timer.getTime() end
  if o.startanim == 0 then
    o.startanim = time
  end
end,

update_anim_frame=function(o,frame,obj)
  o.gfx:update_anim_frame(frame, o.gfxo)
end,

update_frame=function(o,frame)
--  if o.frame == frame then return  end
  o.frame = frame  -- frame virtuelle pas relle
 -- print(o.gfx.imgname..": set frame="..frame)
--  print(o.gfxo)
  o.gfx:update_frame(frame, o.gfxo)
end,

get_frame=function(o)
  --return o.frame -- print(o.gfx.imgname..": set frame="..frame)
--  print(o.gfxo)
  --return o.gfx:get_frame(o.gfxo)
  -- print("get_frame="..o.gfxo.frm)
--  return o.gfxo.frm
  return o.frame
end,

update_none=function(o)
-- print' don't animate
  local i = 0
end,

update_onetime=function(o)
--  print("update_onetime, obj="..o.struct.name)
  if o.startanim ~= 0 and not o.animfinish then
    local ftime = time - o.startanim
    o:update_frame(floor((ftime*o.fps) % #o.gfx.ftable)+1)
    if o.frame == #o.gfx.ftable then o.animfinish = true end
  end
end,

update_onetime_rev=function(o)
--  print("update_onetime_rev, obj="..o.struct.name)
  if o.startanim == 0 then o:update_frame(#o.gfx.ftable) end
  if o.startanim ~= 0 and not o.animfinish then
    local ftime = time - o.startanim
    o:update_frame(#o.gfx.ftable - floor((ftime*o.fps) % #o.gfx.ftable))
-- cas des mesh    o.gfx.mesh:setVertices(o.gfx.vct)
--    print("o.frame="..o.frame)
--    print("#o.gfx.ftable="..#o.gfx.ftable)
    if o.frame == 1 then o.animfinish = true end
  end
end,

update_pingpong=function(o)
  if o.startanim == 0 then o.startanim = time end 
  if o.forward then
    if not o.animfinish then
      o:update_onetime()
    else
      o.forward = false
      o.animfinish = false
      o.startanim = time
    end
  else
    if not o.animfinish then
      o:update_onetime_rev()
    else
      o.forward = true
      o.animfinish = false
      o.startanim = time
    end
  end
--  print(fnum = "fnum)
end,

update_pingpongsin=function(o)
  local ftime = time - o.startanim
  local hf = #o.gfx.ftable/2
  --print("#ftable="..#o.gfx.ftable.." hf="..hf)
--  petite_fleur.gfx.quad:setViewport( (2+floor(2*sin(time*petite_fleur.gfx.frames)))*petite_fleur.gfx.acell.w, 0, petite_fleur.gfx.acell.w,petite_fleur.gfx.acell.h )
  local fnum = floor( hf + hf * sin(ftime * o.fps) + 1 )  -- TODO: à revoir
  o:update_frame(fnum)
end,

update_pingpongcos=function(o)
  --print("update_pingpong_cos "..o.struct.name)
  local ftime = time - o.startanim
  local hf = #o.gfx.ftable/2
  local fnum = floor( hf + hf * cos(ftime * o.fps / pi) + 1 )  -- TODO: à revoir
  o:update_frame(fnum)
end,

update_loop=function(o)
  --print("update_loop "..o.struct.name)
  local ftime = time - o.startanim
  --print("#ftable="..#o.gfx.ftable)
  local tmpf = floor(ftime * o.fps) 
  local fnum = tmpf % #o.gfx.ftable + 1
  --print("fnum="..fnum)
  o:update_frame(fnum)
end,

update_pince=function(o)
  if o.forward then
    if not o.animfinish then o:update_onetime()
    else
      o.forward = false
      o.animfinish = false
      o.startanim = 0
    end
  else
    if not o.animfinish then o:update_onetime_rev()
    else
      o.forward = true
      o.animfinish = false
      o.startanim = 0
    end
  end
end,

-- set the frames animation update function 
set_frameupdate=function(o, f)
  if type(f) == 'function' then o.func = f
  elseif f == "update_loop" then o.func = o.update_loop
  elseif f == "update_pingpong" then o.func =  o.update_pingpong
  elseif f == "update_pingpongsin" then o.func = o.update_pingpongsin
  elseif f == "update_pingpongcos" then o.func = o.update_pingpongcos
  elseif f == "update_onetime" then o.func = o.update_onetime
  elseif f == "update_onetime_rev" then o.func = o.update_onetime_rev
  elseif f == "update_pince" then o.func = o.update_pince
  elseif f == "update_none" then o.func = o.update_none
  else o.func = o.update_none
  end
end,

update=function(o, pos, rot, scale)
  if debug then print(o.struct.name..":update()") end
  if pos ~= nil then o.p = { x=pos.x, y=pos.y} end
  if rot ~= nil then p.rot = rot end
  if scale ~= nil then o.scale = {x=scale.x, y=scale.y} end

--  if o.func ~= nil then
    o:func()
--  end
end,

relativeChange=function(o, pos, rot, scale)
  if pos ~= nil then o.p = { x=o.p.x + pos.x, y=o.p.y + pos.y} end
  if rot ~= nil then p.rot = p.rot + rot end
  if scale ~= nil then o.scale = {x=o.scale.x + scale.x, y=o.scale.y + scale.y} end
end,

compute_coords=function(o, pos, rot, scale)
  local posf,rotf,scalf
  if pos == nil then pos = {x=0,y=0} end
  if rot == nil then rot = 0 end
  if pos.abs == nil then
    posf,rotf =rotate(pos,rot,o)
    if scale == nil then scalef = o.scale
    else scalef = {x=scale.x*o.scale.x, y=scale.y*o.scale.y} end
  elseif pos.abs == "abs" then
    posf = pos
    rotf = rot
    scalef = scale
  elseif pos.abs == "editor" then
    posf = pos
    rotf = o.rot
    scalef = o.scale
  end
  o.posf,o.rotf,o.scalef = posf,rotf,scalef
  return posf,rotf,scalef
end,

predraw=function(o, pos, rot, scale)
 -- print("objinst predraw:"..o.struct.name)
  o.posf,o.rotf,o.scalef = o:compute_coords(pos, rot, scale)
  renderqueue_add(o)
--  renderqueue_add({o.gfxo,posf,rotf,scalef})
end,

finalshadow=function(o)
  local i=1
--  print("objinst finaldraw_shadow:"..o.struct.name)
--  print("posf: x="..o.posf.x..", y="..o.posf.y)
--  print("rotf="..o.rotf)
--  print("scalf: x="..o.scalef.x..", y="..o.scalef.y)
  o.gfx:draw(o.gfxo, o.posf, o.rotf, {x=o.scalef.x, y=o.scalef.y * .5}, {x=.5*o.scale.x,y=0})
--  o.gfx:draw(o.gfxo, o.posf, o.rotf, o.scalef)
  if o.childs ~= nil then
    for i = 1, #o.childs do
      o.childs[i]:draw(o.posf, o.rotf, {x=o.scalef.x, y=o.scalef.y * .5}, {x=.5*o.scale.x,y=0})
    end
  end
end,

finaldraw=function(o)
--  print("objinst finaldraw:"..o.struct.name)
--  print("posf: x="..o.posf.x..", y="..o.posf.y)
--  print("rotf="..o.rotf)
--  print("scalf: x="..o.scalef.x..", y="..o.scalef.y)
  o.gfx:draw(o.gfxo, o.posf, o.rotf, o.scalef)
  if o.childs ~= nil then
    for i = 1, #o.childs do
      o.childs[i]:draw(o.posf, o.rotf, o.scalef)
    end
  end
end,

draw=function(o, pos, rot, scale)
 -- print("objinst draw:"..o.struct.name)
  local posf,rotf,scalf = o:compute_coords(pos, rot, scale)
  o.gfx:draw(o.gfxo, posf, rotf, scalef)
--  renderqueue_add({o.gfxo,posf,rotf,scalef})
  if o.childs ~= nil then
    for i = 1, #o.childs do
      o.childs[i]:draw(posf, rotf, scalef)
    end
  end
end,

draw_vector=function(o, src, dest)
  lg.setColor(0,.2,1,.7)
  lg.circle("line",src.x, src.y, 5)
  lg.line(src.x, src.y, dest.x, dest.y)
  if debug then print("draw_vector o.p.x="..o.p.x.." .y="..o.p.y..
        " dest.x="..dest.x.." .y="..dest.y) end
end,

finaldraw_debug=function(o) -- a finir
--  print("objinst finaldraw:"..o.struct.name)
--  print("posf: x="..o.posf.x..", y="..o.posf.y)
--  print("rotf="..o.rotf)
--  print("scalf: x="..o.scalef.x..", y="..o.scalef.y)
  if o.childs ~= nil then
    for i = 1, #o.childs do
      o.childs[i]:draw(o.posf, o.rotf, o.scalef)
    end
  end
end,

draw_debug=function(o, pos, rot, scale)
  print("draw_debug:"..o.struct.name)
 
  local posf,rotf,scalf = o:compute_coords(pos, rot, scale)
--  print(" posf.x="..posf.x.." y="..posf.y.." rotf="..rotf)

  o.gfx:draw_debug(o.gfxo, posf, rotf, scalef)
  if o.childs ~= nil then
    for i = 1, #o.childs do
      local v2,rot=rotate(posf,rotf,o.childs[i])
      o:draw_vector(posf,v2) -- o.childs[i].p)
      o.childs[i]:draw_debug(posf,rotf,scalef)
    end
  end

  -- position
  lg.setColor(0,0,1,.7)
  lg.circle('fill',posf.x,posf.y,5)

  -- current frame
  etiquette_draw(o.p,o:get_frame())
  etiquette_draw({x=o.p.x, y=o.p.y+12},o.struct.name)
end,

} setmetatable(objinst,objinst)
objinst.__index=objinst
