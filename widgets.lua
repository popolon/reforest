require "helpers"

fullscreen=false

alert=nil
widgs={}
editmod=nil
font=nil
etiquette=nil
W,H=1280,768

function widgets_load()
  if debug then print("init widgets") end
  font = love.graphics.getFont()
  etiquette = love.graphics.newText(font, "")
  
  love.window.setFullscreen(fullscreen)
  if not fullscreen then
   love.window.setMode(W,H,{resizable=false})
  end
end

function button_toggle_create(text,flag,coords)
  return {text=texte, x=coords.x,y=coords.y, w=coords.w,h=coords.h, flag=flag, isin=false}
end

function button_toggle_update(o)
  if isinrec({x=mx,y=my},o) then o.isin = true
    if lmb and not olmb then _G[o.flag] = not _G[o.flag] return end
  else o.isin = false end
end

function button_toggle_destroy(o)
  o=nil
end

function button_toggle_draw(o)
  if o.isin == false then
    lg.setColor(.2,.2,.2,1)
  else
    lg.setColor(.4,.4,.4,1)
  end
  lg.rectangle("fill",o.x,o.y,o.w,o.h)
  lg.setColor(o.col)
  lg.print(o.text, o.x+5, o.y+2)  
end

function button_delete(o)
  o=nil
end

function button_update(o)
  if isinrec({x=mx,y=my},o) then o.isin = true
    if lmb and not olmb then o.func() return end
  else o.isin = false end
end

function button_draw(o)
  if o.isin == false then
    lg.setColor(.2,.2,.2,1)
  else
    lg.setColor(.4,.4,.4,1)
  end
  lg.rectangle("fill",o.x,o.y,o.w,o.h)
  lg.setColor(o.col)
  lg.print(o.text, o.x+5, o.y+2)
end


function alert_create(text,func,coords)
  if alert ~= nil then alert_destroy() return end
  local co = coords
  if co == nil then
    local mtmp={x=mx-50,y=my-50}
    co={x=mx-50,y=my-50,w=150,h=100}
    if mx-50 < 0 then co.x=0
    elseif mx-50+co.w > W then co.x = W-co.w end
    if my-50 < 0 then co.y=0
    elseif my-50+co.h > H then co.y = H-co.h end
  end
  alert = {type="alert", text=text, co=co, 
     yes={text="O", x=co.x+co.w-50,y=co.y+co.h-30, w=30,h=20, col={.5,1,.5,1}, isin=false},
     no={text="X", x=co.x+20,y=co.y+co.h-30, w=30,h=20, col={1,.5,.5,1}, isin=false},
     func=func,
     cur=no
  }
  return alert
end

function alert_destroy(widg)
  alert=nil
end

function alert_update()
  -- O
  if isinrec({x=mx,y=my},alert.yes) then alert.yes.isin = true
    if lmb and not olmb then olmb=lmb alert.func() return end
  else alert.yes.isin = false end
  -- X
  if isinrec({x=mx,y=my},alert.no) then alert.no.isin = true
    if lmb and not olmb then olmb=lmb alert=nil return end
  else alert.no.isin = false end
end

function alert_draw()
  local co,text=alert.co,alert.text 

  lg.setColor(0,0,0,.7)
  lg.rectangle("fill",co.x,co.y,co.w,co.h)
  lg.setColor(.2,.2,.2,.7)
  lg.rectangle("fill",co.x,co.y,co.w,12)
  lg.setColor(1,1,1,1)
  lg.print("!!!!",co.x+20,co.y)
  
  lg.print(text,co.x+20,co.y+20)

  button_draw(alert.no)
  button_draw(alert.yes)
end

function nbuttons_create(pos,structs)
  
end

function nbuttons_update()
end
function nbuttons_draw()
end

function etiquette_draw(pos,txt)
    etiquette:set(txt)
    local txtw,txth=etiquette:getDimensions()
--    print("instance["..i.."]="..instances[i].struct.name)
    lg.setColor(1,1,1,.7)
    lg.rectangle("fill",pos.x+3,pos.y-12,txtw+4,txth+4)

    lg.setColor(0,0,0,.7)
    lg.draw(etiquette,pos.x+5,pos.y-10)
end
