m=math cos=m.cos sin=m.sin

renderqueue={}

function isinrec(p,rec)
  if p.x > rec.x and p.x < rec.x+rec.w and
     p.y > rec.y and p.y < rec.y+rec.h then
  return true
  else return false end
end

function isnear(p1,p2,r)
  if (p1.x-p2.x)^2 + (p1.y-p2.y)^2 < r*r then
  return true
  else return false end
end
-- rot(o.p,o.rot,o.childs[i]
function rotate(p, rot, o)
 local c,s=cos(rot),sin(rot)
 return {x=p.x+o.p.x*c-o.p.y*s,
	 y=p.y+o.p.x*s+o.p.y*c},
	   rot+o.rot
end

function dfind(buf,val)
  local l,M,m=1,#buf,0
  while l<=M do
    m=floor((l+M)/2)
    if buf[m].posf.y<val then l=m+1
    else M=m-1 end
  end
  return M+1
end

function spra(spr,o)
  local i=dfind(sprites,o.y)
  table.insert(sprites,o)
end

function renderqueue_clean()
  renderqueue={}
end

function renderqueue_add(o)
  local i=dfind(renderqueue, o.posf.y)
  table.insert(renderqueue,i,o)
end

function renderqueue_draw()

-- le fonctions doivent être définies dans (lovel.lua, objinst.lua)
-- premier pass => shadows
  love.graphics.setColor(.2,.2,.5,.2)
  for i=1,#renderqueue do
    renderqueue[i]:finalshadow()
    --lg.ellipse("fill", renderqueue[i].posf.x,renderqueue[i].posf.y, 100,50)
  end

-- second pass => objets themselves
  love.graphics.setColor(1,1,1,1)
  for i=1,#renderqueue do 
    renderqueue[i]:finaldraw()
-- U = renderqueue[i]    lg.draw(renderqueue[i]:....).
--    table.remove(renderqueue,i)
  end
end
