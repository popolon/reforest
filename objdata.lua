-- type d'instance 
-- * animmesh (deformable mesh)
-- * animquad (simple quad)


chenille_gfx_struct={
  inst = nil,
  name = "chenille.png",
  ftable = {1},
  acell={w=512,h=118},
  adir="H",
  type="meshani",
  meshi={ -- mesh infos (mesh=love mesh type
    dir="H",
    steps=8,
  },
}
chenille_struct={
  name="chenille_struct",
  gfx_struct = chenille_gfx_struct,
  p={x=300,y=700}
}

tronc_gfx_struct={
  inst = nil,
  name = "tronc.png",
  ftable = {1},
  acell={w=512,h=128},
  adir="H",
  type="quadani"
}
tronc_struct={
  name="tronc_struct",
  gfx_struct = tronc_gfx_struct,
}
  
buisson_gfx_struct={
  inst = nil,
  name = "buisson1_texture.png",
  ftable = {1},
  acell={w=256,h=256},
  ctr={x=128,y=240},
  adir="H",
  type="quadani"
}

buisson_struct={
  name="buisson_struct",
  gfx_struct = buisson_gfx_struct,
  p={x=200,y=350},
}

buisson2_struct={
  name="buisson2_struct",
  gfx_struct = buisson_gfx_struct,
  p={x=500,y=550},
}

vulvia_gfx_struct={
  inst = nil,
  name = "vulviasheet.png",
  ftable={1,2,3,4,5},
  acell={w=120,h=280},
  adir="H",
  type="quadani"
}

vulvia_struct={
  name="vulvia_struct",
  gfx_struct = vulvia_gfx_struct,
  
  startanim = 0,
  animi = { --déjà utilisé
    fps = 5,
    func = "update_pingpongcos"
  },
  p={x=800,y=750}
}

-- 1,4,7,10,13
perso2_gfx_struct={
  inst = nil,
  name = "marche2sheet.png",
  seqs={
    {
      ftable = {1},
      fps = 12,
      func = "update_loop",
    },
    {
      ftable = {2,2,2,3,3,3,4,4,4,5,5,5},
      fps = 12,
      func = "update_loop",
    }
  },
  ftable = {2,2,2,3,3,3,4,4,4,5,5,5},
  acell = {w=152,h=240},
  ctr={x=76,y=220},
  adir="H",
  type="quadani",
}

perso2_struct={
  name="perso2_struct",
  gfx_struct = perso2_gfx_struct,
  startanim = 0,
  animi = {
    fps = 12,
    func = "update_loop",
  },
  p={x=500,y=750},
  vmax=3,
  acc=0.05
}


perso_gfx_struct={
  inst = nil,
  name = "marchesheet.png",
  ftable = {1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5},
  acell = {w=256,h=432},
  ctr={x=90,y=410},
  adir="H",
  type="quadani",
--  marche_shifts = {{x=48,y=160},{x=48,y=160},{x=48,y=160},{x=48,y=160},{x=64,y=160},{x=64,y=160},{x=64,y=160},{x=64,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160},{x=112,y=160}},
}

perso_struct={
  name="perso_struct",
  gfx_struct = perso_gfx_struct,
  startanim = 0,
  animi = {
    fps = 12,
    func = "update_loop",
  },
  p={x=750,y=400},
  vmax=3,
  acc=0.05
}


petite_fleur_gfx_struct={
  inst = nil,
  name="petite_fleursheet.png",
--petite_fleur.anim.table={1,1,1,4,4,4,7,7,7,10,10,12,12,12}
  ftable={1,1,1,2,2,2,3,3,3,4,4,5,5,5},
  acell={w=64,h=64},
  adir="H",
  type="quadani",
  ctr={x=31,y=30}
}

petite_fleur_struct={
  name="petite_fleur_struct",
  gfx_struct = petite_fleur_gfx_struct,
  startanim = 0,
  animi = { --déjà utilisé
    fps = 6,
    func = "update_pingpong"
  },
  p={x=650,y=350}
}

poignet_gfx_struct={
  inst = nil,
  name="poignetsheet.png",
  ftable={1,1,1,2,2,2,3},
  acell={w=320,h=128},
  adir="V",
  type="quadani",
--  ctr={x=32,y=64}
  ctr={x=238,y=92},
}

poignet_struct={
  name="poignet_struct",
  gfx_struct = poignet_gfx_struct,
  startanim = 0,
  animi = { --déjà utilisé
    fps = 48,
    forward = true,
    start_anim = false,
    func = "update_pince"
  },
  p={x=0,y=0},
  v={x=0,y=0}
}

beeaile_gfx_struct={
  inst = nil,
  name="bee_aile.png",
  ftable={1},
  acell={w=128,h=64},
  adir="H",
  type="quadani",
  ctr={x=120,y=48}
}

beeaile_struct={
  name="beeaile_struct",
  gfx_struct = beeaile_gfx_struct,
  p={x=10,y=-74}
}
  
bee_gfx_struct={
  inst = nil,
  name="bee.png",
  ftable={1},
  acell={w=192,h=128},
  adir="H",
  type="quadani",
  ctr={x=98,y=128}
}

bee_struct={
  name="bee_struct",
  gfx_struct = bee_gfx_struct,
  childs={beeaile_struct},
  p={x=300,y=500}
}

fleur_gfx_struct={
  inst = nil,
  name = "fleursheet.png",
--fleur.anim.table={1,1,3,3,3,6,6,8,8,8,11,11,13,13,15,15,16}
  ftable = {1,1,2,2,2,3,3,4,4,4,5,5,6,6,7,7,8,8,8,8},
  acell={w=128,h=128},
  adir="H",
  meshi={ -- mesh infos (mesh=love mesh type
    dir="V",
    steps=1,
  },
  type="meshani",
}

fleur_struct={
  name="fleur_struct",
  gfx_struct = fleur_gfx_struct,
  startanim = 0,
  animi = { --déjà utilisé
    fps = 16,
    func = "update_onetime"
  },
  p={x=250,y=250}
}
