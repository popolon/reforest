require "quadani"
require "meshani"
require "objinst"
require "level"
require "widgets"

m=math sin=m.sin cos=m.cos floor=m.floor abs=m.abs
pi=m.pi pi2=pi/2 pi4=pi/4 pi8=pi/8
lg=love.graphics
kd=love.keyboard.isDown

omx, omy = 0,0
mx, my = 0,0
olmb,lmb = love.mouse.isDown(1),love.mouse.isDown(1)
ormb,rmb = love.mouse.isDown(2),love.mouse.isDown(2)
ommb,mmb = love.mouse.isDown(3),love.mouse.isDown(3)
owmx,owmy,wmx,wmy= 0,0,0,0

vitesse={x=0,y=0}

W=1024 H=768

debug=false
debugLvl=1
dbr=20
param=false
hard_head=true
grabed=nil

--ix,iy=0,0
--w,h=4*16,12*16

-- * table Image + animation, plus tailles cell...
require "objdata"


-- A lier à l'objets
chenille={}
fleur={}
petite_fleur={}
bee={}
cubegel={}
tronc={}
buisson={}
buisson2={}
vulvia={}
poignet={}
perso={}
persog={}

instances={}
current_instance=1
curlvl=nil

w1,w2=0,0
--h1,h2=0,0
celrap=1
-- pas utilisé ici
--cx,cy=32,208-16
--bw=64 bw2=32
-----------

time = love.timer.getTime()
v3={}

function obj_load(struct)
  if debug then print('objload ------')
    if struct== nil then print("struct nil")
    else print(struct.name) end 
  end
  local o = objinst(struct)
  table.insert(instances,o)
  return o
end

function bee_update()
--   bee.p = {x =300+20*cos(time), y=450+20*sin(time)}
--   bee.rot = bee.rot+0.05
   if bee.childs ~= nil then
     bee.childs[1].rot = pi4*(floor(time*30)%3) -- filp de l'aile
   end
end

function poignet_load()
  poignet = obj_load(poignet_struct)
  poignet.select = nil --grabed
  -- ajouter aux instances avec un parametres particulier (curseur ?)
end

function poignet_contact()
  if lmb and not olmb then
    poignet:start_anim()
    for i = 1, #instances do local I= instances[i]
      if I.struct.name ~= "poignet_struct" -- don't want to grab itself
--      and I:touchCenter({x=mx,y=my},dbr)
      and I:touch({x=mx,y=my},dbr)
--      ((I.p.x-mx)^2 + (I.p.y-my)^2) < dbr*dbr
      then
        grabed = I
        break
      end
    end
  elseif lmb and grabed ~= nil then
    grabed.p={x=grabed.p.x+(mx-omx), y=grabed.p.y+(my-omy)}
  elseif not lmb and olmb then
    poignet.forward = false
    poignet:start_anim()
    grabed=nil
  end
end

function poignet_update()
  local Po=poignet
  poignet:update()

  if mx-omx ~= 0 then poignet.v.x = poignet.v.x+(mx-omx)/3
  else poignet.v.x = poignet.v.x*0.9 end
  if my-omy ~= 0 then poignet.v.y = poignet.v.y+(mx-omx)/3
  else poignet.v.y = poignet.v.y*0.9 end
  local add=0
--  if grabed == nil then --floating
--    add=20*sin(time*2)
--  end
  poignet.p = {x=mx,y=add+my}
  poignet.rot = pi4 - poignet.v.x/300
  local a = poignet.rot
  if a > pi2 and a < pi+pi2 then
    poignet.rot = a + poignet.v.y/300
  elseif a < pi2 or a > pi+pi2 then
    poignet.rot = a - poignet.v.y/300
  end
end

function love.load()
  widgets_load()

  curlvl = level(level1)
 
  -- TODO : passer tout ça dans le level 
  bee = obj_load(bee_struct)
  chenille = obj_load(chenille_struct)
  love.window.setMode(W,H,{resizable=true})
  fleur = obj_load(fleur_struct)
  buisson = obj_load(buisson_struct)
  buisson2 = obj_load(buisson2_struct)
  vulvia = obj_load(vulvia_struct)
  petite_fleur = obj_load(petite_fleur_struct)
  perso = obj_load(perso2_struct)
  persog = obj_load(perso_struct)
  poignet_load()
end

function game_keypressed(key)
  if key == "lshift" or key == "rshift" then
    debug = not debug
    --tir objEdMod = objEdMod % #objEdMods + 1
  end
end

--- deplacement personnage principal
function game_keydown()
  local o=perso
  if kd('left') then
    if o.v.x > -o.vmax then
      if o.v.x > 0 then o.v.x = o.v.x*o.decel 
      else o.scale.x=-1
      end
      o.v.x = o.v.x-o.acc
    else  o.v.x = -o.vmax end
  elseif kd('right') then
    if o.v.x < 0 then o.v.x = o.v.x*o.decel
    else o.scale.x=1
    end
    if o.v.x < o.vmax then o.v.x = o.v.x+o.acc
    else  o.v.x = o.vmax end
  else
    o.v.x = o.v.x*o.decel
    if abs(o.v.x) < o.acc then o.v.x=0 end
  end
  
  if kd('up') then
    if o.v.y > 0 then o.v.y = o.v.y*o.decel end
    if o.v.y > -o.vmax then o.v.y = o.v.y-o.acc
    else  o.v.y = -o.vmax end
  elseif kd('down') then
    if o.v.y < 0 then o.v.y = o.v.y*o.decel end
    if o.v.y < o.vmax then o.v.y = o.v.y+o.acc
    else  o.v.y = o.vmax end
  else
    o.v.y = o.v.y*o.decel
    if abs(o.v.y) < o.acc then o.v.y=0 end
  end
  o.p.x=o.p.x+o.v.x
  if o.p.x < 0 then o.p.x=0 o.v.x=0 
  elseif o.p.x > curlvl.w*64+63 then o.p.x = curlvl.w*64+63 o.v.x=0 end

  o.p.y=o.p.y+o.v.y
  if o.p.y < 0 then o.p.y=0 o.v.y=0 
  elseif o.p.y > curlvl.h*64+63 then o.p.y = curlvl.h*64+63 o.v.y=0 end

  if o.v.x ~= 0 or o.v.y ~= 0 then
    perso:set_frameupdate('update_loop')
  else
    perso:set_frameupdate('update_none')
    perso:update_anim_frame(1,perso.gfxo)
  end

end

hard_head=true
function chenille_ondule()
  local mv = cos(-time*5)
  --if mv>0 then ox=ox+absmv end
  chenille.p.x = (chenille.p.x + abs(mv))%1024
--  chenille.p.y=oy

  local step,steps=chenille.gfx.meshi.step,chenille.gfx.meshi.steps
--  local step=16
  local ending=steps+1
  local h2 = chenille.gfx.acell.h
  -- stop at steps, don't move last one
  if hard_head then ending=steps end
  vct=chenille.gfxo.vct
  for i = 1,ending do local v1,v2=vct[i*2-1],vct[i*2]
   --local angle=-time*5+(i-1/steps)/2
   local angle=-time*5+i/2
   local cosA,sinA=cos(angle),sin(angle)
   v1[1]=(i-1)*step +30*cosA
   v1[2]=-30*sinA
   v2[1]=(i-1)*step +20*cosA
   v2[2]=h2-5*sinA
  end
  if hard_head then
    -- last one move in concert with previous one to not deform the head
    -- i=steps -1 to compute the same base, but apply to i+1 vertices 
    local i = steps
    --local angle = -time*5+(i-1/steps)/2
    local angle = -time*5+i/2
    local v1,v2 = vct[(steps)*2-1],vct[(steps)*2]
    local cosA,sinA=cos(angle),sin(angle)
    v1[2]=-5*sinA
    v2[2]=h2-5*sinA
    -- over simplify, the normal of the previous segment angle 
    -- should be computed to move verticaly
    -- P=(v.y,-v.x)
    -- Y=(v1[1]-v2[1])*celrap v4[2]v1[2]-v2[2]*2
    local v3,v4=vct[(steps+1)*2-1],vct[(steps+1)*2]
    local add=(v1[1]-v2[1])*celrap
    v3[1]=v1[1]+step v4[1]=v2[1]+step
    v3[2]=v1[2]+add v4[2]=v2[2]+add
  end
  chenille.gfx:update_mesh(chenille.gfxo)
end

function game_update()
  if debug then print("updates start")end
  curlvl:update()
  for i=1,#instances do
    if debug then print("update: instance "..i.."="..instances[i].struct.name) end
    instances[i]:update()
  end
  chenille_ondule()
  bee_update()
  poignet_update()
  poignet_contact()
end

function game_draw()
  renderqueue_clean()

--  lg.clear(.4,.5,.3,1)
  curlvl:draw() -- Background

--  lg.setColor(1,0,1,1)

--  lg.setColor(0,0,.5,.3) --shadow
--  chenille:draw(nil,nil,{x=1.1,y=1.1})

  lg.setColor(1,1,1,1)
  chenille:predraw()
  if debug then chenille:draw_debug() end

  lg.setColor(1,1,1,1)
  fleur:predraw()
  if debug then fleur:draw_debug() end

  lg.setColor(1,1,1,1)
  vulvia:predraw()
  if debug then vulvia:draw_debug() end

  lg.setColor(1,1,1,1)
  petite_fleur:predraw()
  if debug then petite_fleur:draw_debug() end

  if grabed ~= nil and  grabed.struct.name == bee.struct.name then
    lg.setColor(1,.5,.5,.7)
  else
    lg.setColor(1,1,1,1)
  end
  --   bee.p = {x =300+20*cos(time), y=450+20*sin(time)}

  local bmv={x=20*cos(time), y=20*sin(time)}
  bee:predraw(bmv)
  if debug then bee:draw_debug(bmv) end

  lg.setColor(1,1,1,1)
  buisson:predraw()
  if debug then buisson:draw_debug() end
  buisson2:predraw()
  if debug then buisson2:draw_debug() end

  lg.setColor(1,0,1,.7)
  lg.circle('line',poignet.p.x,poignet.p.y,dbr)

  lg.setColor(1,1,1,1)
  persog:predraw()
  perso:predraw()
  if debug then perso:draw_debug() end
  
  lg.setColor(1,1,1,1)
  poignet:predraw()
  if debug then poignet:draw_debug() end
  
  renderqueue_draw()
  
  if debug then
    lg.setColor(0,0,0,.3)
    lg.rectangle('fill',0,0,300,64)
    lg.setColor(1,1,1,1)
    if grabed ~= nil then
      lg.print("grabed = "..grabed.struct.name.." ps.x="..grabed.p.x..", y="..grabed.p.y,150,0)
    else
      lg.print("grabed = nil",150,0)
    end
    lg.print("mx="..mx.." my="..my,0,0)
    lg.print("omx="..omx.." omy="..omy,0,12)
    lg.print("poignet.forward="..tostring(poignet.forward),0,24)
  end
end

grabed = nil
dec=0
minnbr=5
selW,selH = W/minnbr, 100
selR = selW/selH
selX,selY = 0, H-selH
objEdMods={ "move center", "rotate", "scale"}
objEdMod=1

function objeditor_selected()
  if current_instance <= dec then
    dec = current_instance - 1
  elseif current_instance - dec > minnbr and dec + minnbr < #instances then 
    dec = current_instance - minnbr
  end
end

function objeditor_keypressed(key)
-- print("function editor_keypressed")
  local o=instances[current_instance]
  if key == 'space' then
    current_instance = current_instance % #instances+1
    objeditor_selected()

  elseif key == "lshift" then
    objEdMod = objEdMod % #objEdMods + 1

  elseif key == 'm' then -- move centre
    objEdMod = 1
  elseif key == 'r' then -- rotate
    objEdMod = 2
  elseif key == 's' then -- scale
    objEdMod = 3

  elseif key == "left" then
   o.gfxo.ctr.x=o.gfxo.ctr.x-1
  elseif key == "right" then
   o.gfxo.ctr.x=o.gfxo.ctr.x+1
  elseif key == "up" then
   o.gfxo.ctr.y=o.gfxo.ctr.y-1
  elseif key == "down" then
   o.gfxo.ctr.y=o.gfxo.ctr.y+1
  end
end

function objeditor_keydown()
end

function objeditor_update(dt)
  local coord={x=W/2,y=H/2,abs=true}
  local i=instances[current_instance]
  if grabed ~= nil and lmb then
    if objEdMods[objEdMod] == "move center" then
      grabed.gfxo.ctr={x=grabed.gfxo.ctr.x-(mx-omx), y=grabed.gfxo.ctr.y-(my-omy)}
    elseif objEdMods[objEdMod] == "scale" then
     grabed.scale = { x= grabed.scale.x + (mx-omx)/100, y= grabed.scale.y + (my-omy)/100 }
    elseif objEdMods[objEdMod] == "rotate" then
     grabed.rot = grabed.rot + (mx-omx)/100
    end
  elseif lmb and not olbm then
    if ((coord.x-mx)^2 + (coord.y-my)^2) < dbr*dbr then
      grabed = instances[current_instance]
    elseif my > selY and my < selY+selH then
      current_instance = floor(mx / selW)+dec+1
    end
  elseif not lmb and olmb then
    grabed=nil
  elseif not lmb and owmy ~= wmy then
    if my >= selY then
      current_instance = (current_instance - 1 - (wmy-owmy)) % #instances + 1
      owmy=wmy
      objeditor_selected()
    end
  end
end

function objeditor_draw()
  local coord={x=W/2,y=H/2,abs="editor"}
  lg.setColor(1,1,1,1)

  local o=instances[current_instance]
  lg.rectangle("line",coord.x-o.gfxo.ctr.x,coord.y-o.gfxo.ctr.y, 
                o.gfx.acell.w, o.gfx.acell.h)

  o:update()
  o:draw({x=coord.x,y=coord.y,abs="editor"})
  lg.setColor(1,(time*8)%2,(time*8)%2,.7)
  lg.circle("line",coord.x,coord.y,dbr)
  lg.line(coord.x,0,coord.x,selY)
  lg.line(0,coord.y,W,coord.y)

  if ((coord.x-mx)^2 + (coord.y-my)^2) < dbr*dbr then
    lg.circle("fill",coord.x,coord.y,dbr)
  end
  lg.setColor(1,1,1,1)
  local Ypos=0
  lg.print("acell.w="..o.gfx.acell.w..",h="..o.gfx.acell.h,0,Ypos) Ypos = Ypos + 12
  lg.print("ctr.x="..o.gfxo.ctr.x..",y="..o.gfxo.ctr.y,0,Ypos) Ypos = Ypos + 12
  lg.print("mx="..mx.." my="..my.."  distance: x="..abs(mx-coord.x)..", y="..abs(my-coord.y),0,Ypos) Ypos = Ypos + 12
  lg.print("lmb:"..tostring(lmb).." rmb:"..tostring(rmb).." mmb:"..tostring(mmb),0,Ypos) Ypos = Ypos + 12
  lg.print("wmx:"..wmx.." wmy:"..wmy,0,Ypos) Ypos = Ypos + 12
  lg.print("objEdMod: "..objEdMods[objEdMod],0,Ypos) Ypos = Ypos + 12


 ---- boutons/miniatures
   -- TODO: clipping alterné avec trop grand toutes les secondes?
  for i = 0, minnbr-1 do local obj = instances[i+dec+1]
    if i+dec+1 == current_instance then
      lg.setColor(.5,.5,.5,1)
      lg.rectangle("fill",selX+i*selW,selY,selW,selH)
    end
    lg.setColor(1,1,1,1)
    lg.rectangle("line",selX+i*selW,selY,selW,selH)
--    instances[i+dec+1]:draw({x=selX+i*selW+selW/2,y=selY+selH/2},nil,{x=.25,y=.25})
    local objR,escR = obj.gfx.acell.w/obj.gfx.acell.h,0
    if objR > selR then -- rapport  obj +large que cellule
      escR = selW/obj.gfx.acell.w
    else -- rapport  cellule +large que obj
      escR = selH/obj.gfx.acell.h
    end
    local posX = selX+i*selW+selW/2+escR*(obj.gfxo.ctr.x-obj.gfx.acell.w/2) -- /obj.gfx.acell.h
    local posY = selY+selH/2+escR*(obj.gfxo.ctr.y-obj.gfx.acell.h/2)  -- /obj.gfx.acell.h
    instances[i+dec+1]:draw({x=posX,y=posY,abs="abs"},0,
       {x=escR,y=escR})
    lg.setColor(1,0,1,1)
    lg.circle("line", posX,posY, 3)
  end

  lg.print("obj:"..instances[current_instance].struct.name,150,0)
  if grabed ~= nil then
    lg.print("grabed = "..grabed.struct.name,150,12)
  else
    lg.print("grabed = nil",150,12)
  end
end

modes={
  { name = 'game', keypressed = game_keypressed, keydown= game_keydown, input = game_input, update = game_update, draw = game_draw },
  { name = 'objeditor', keypressed = objeditor_keypressed, keydown=objeditor_keydown, input = objeditor_input, update = objeditor_update, draw = objeditor_draw },
--  { name = 'menu', keypressed = menu_keypressed, keydown=menu_keydown, input = menu_input, update = menu_update, draw = menu_draw },
}
mode = 1

function love.keypressed(key)
  if key == 'escape' then
    alert_create('->[]', love.event.quit)
--    love.event.quit()
  elseif key == 'f' then
    fullscreen = not fullscreen
    love.window.setFullscreen(fullscreen)
    if not fullscreen then
     love.window.setMode(W,H,{resizable=false})
        end
  elseif key == 'return' then
    if alert ~= nil then alert.func()
    else fleur:start_anim() end
  elseif key == 'tab' then
--    debug = not debug
    mode = (mode % #modes) + 1
    grabed = nil
   --elseif key == 'lshift' then
  end
  modes[mode].keypressed(key)
end

function love.update(dt)
  time = love.timer.getTime()
	
  omx,omy = mx,my
  olmb,ormb,ommb = lmb,rmb, mmb
  owmb1,owmb2 = wmb1,wmb2
  mx, my = love.mouse.getPosition()
  lmb = love.mouse.isDown(1)
  rmb = love.mouse.isDown(2)
  mmb = love.mouse.isDown(3)
  wmb1 = love.mouse.isDown(5)
  wmb2 = love.mouse.isDown(6)
  --if debug then print("update = "..modes[mode].name) end
  modes[mode].keydown(dt)
  modes[mode].update(dt)
  if alert ~= nil then
    alert_update()
  end
end

function love.wheelmoved( dx, dy )
  owmx,owmy = wmx,wmy
  wmx,wmy = wmx+dx,wmy+dy
end

function love.draw()
  -- if debug then print("draw = "..modes[mode].name) end
  modes[mode].draw()
  if alert ~= nil then
    alert_draw()
  end
end
