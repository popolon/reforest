require "helpers"
require "widgets"
rand=math.random
lg=love.graphics

--require "lvldata"


-- TODO mettre les obtiels comme des objets ordinaires.


bg={}
player={}
agents={}
objs={}
blts={}

bgtiles="assets/bgtiles.png"
gtilesquad={}
otilesquad={}

Ojbsc=1.2

level1={
  imagename="bgtiles.png",
  w=20,
  h=12,
  bgtiles={{0,0},{64,0},{128,0},{192,0},{256,0},{256,64}},
  obtiles={{x=0,y=64,w=64,h=64,cx=32,cy=60},{x=64,y=64,w=64,h=3*64,cx=32,cy=3*64-4}},
  clearcolor={53/255,117/255,40/255,1}
}

-- convertir ça en veritables objets
bgobj={
__call=function(o, struct)
  local o={} setmetatable(o,bgobj)
  o.struct = struct
  o.image=struct.image
  o.quad=struct.quad
  o.posf=struct.posf
  o.ctr=struct.ctr
  return o
end,

finaldraw=function(o)
 -- + (32,32) => au milieu de la tuile
 -- texture of the obj, quad, x/y, rotation, scale x,y
  lg.draw(o.image,o.quad,  o.posf.x-o.ctr.x*Ojbsc+32, o.posf.y-o.ctr.y*Ojbsc+32, 0 , Ojbsc,Ojbsc)
--  lg.circle('fill',o.posf.x+32, o.posf.y+32,3,3)
end,

finalshadow=function(o)
 -- + (32,32) => au milieu de la tuile
  -- texture of the obj, quad, x/y, rotation, scale x,y, offset x,y, shear.x(/y)
  lg.draw(o.image,o.quad,  o.posf.x-o.ctr.x*Ojbsc+32, o.posf.y-o.ctr.y*Ojbsc+32, 0 , Ojbsc,Ojbsc*.5, 0,0, .5)
--  lg.circle('fill',o.posf.x+32, o.posf.y+32,10,10)
end,


} setmetatable(bgobj,bgobj)
bgobj.__index=bgobj


level={
-- certains paramètres peuvent être dans l'animation
-- puis écrasé par l'instance et réciproquement?
__call=function(o, struct)
  local o={} setmetatable(o,level)
  o.struct = struct
  o.w=struct.w -- nombre de tuiles
  o.h=struct.h
  o.imagename = struct.imagename
  o.image = lg.newImage("assets/"..o.imagename)
  o.iW,o.iH = o.image:getWidth(),o.image:getHeight()

  o.clearcolor=struct.clearcolor
  o.gtiles={} --ground tiles
  o.bgtiles=struct.bgtiles
  o.nbgtiles=#struct.bgtiles

  -- TODO à traiter comme des objets unifiés et non plus des tuiles
  o.otiles={} -- object tiles
  o.obtiles=struct.obtiles
  o.nobtiles=#struct.obtiles

 --- definit les quads graphics
  for i=1,o.nbgtiles do
    if debug then print("bgtiles["..i.."]= "..o.bgtiles[i][1]..", "..o.bgtiles[i][2]) end
    gtilesquad[i] = lg.newQuad(o.bgtiles[i][1],o.bgtiles[i][2],  64,64, o.image:getDimensions())
  end
  for i=1,o.nobtiles do
    otilesquad[i] = lg.newQuad(o.obtiles[i].x,o.obtiles[i].y,  o.obtiles[i].w,o.obtiles[i].h, o.image:getDimensions())
  end
  
  -- init bg/ob
  for y=1,o.h do
    o.gtiles[y]={}
    o.otiles[y]={}
  end
  for i = 0,200 do
    o.gtiles[rand(1,o.h)][rand(1,o.w)]=rand(1,o.nbgtiles)
  end
  for i = 0,10 do
    local tmpx,tmpy=rand(1,o.w),rand(1,o.h) -- position
    local tmpquad=rand(1, o.nobtiles)
    local tmpobj=bgobj( {posf={x=tmpx*64,y=tmpy*64}, ctr={x=o.obtiles[tmpquad].cx,y=o.obtiles[tmpquad].cy}, image=o.image, quad=otilesquad[tmpquad]} )
    table.insert(objs,tmpobj)
    o.otiles[tmpy][tmpx] = tmpobj
  end

-- portes I/O
--  o.indoor={x=1,y=10}
--  o.tiles[indoor.y][indoor.x]="indoor"
--  o.tiles[outdoor.y][outdoor.x]="outdoor"
  return o
end,

update=function(o)
end,

draw=function(o)
  lg.clear(o.clearcolor) -- TODO mettre dans le mixer
  lg.setColor(1,1,1,1) -- TODO mettre dans le mixer
  for y=1,o.h do
    for x=1,o.w do
      if o.gtiles[y][x] ~= nil then
        if debug then print("o.gtiles["..y.."]["..x.."]="..o.gtiles[y][x])end
        lg.draw(o.image, gtilesquad[o.gtiles[y][x]], (x-1)*64,(y-1)*64)
      end
    end
  end

  for i=1,#objs do
    renderqueue_add(objs[i])
  end
end,

drawshadow=function(o)
-- TODO
end,

} setmetatable(level,level)
level.__index=level
