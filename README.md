This is a game about "World regrow" by reforesting earth, made with LÖVE engine.

W.I.P. it include some tools including:

To launch it, in a terminal:

git clone https://framagit.org/popolon/reforest
love reforest

You can :
* Move the main character with arrows
* Move the hand with mouse and grap (most) objects to move them.

In Game:
* Debugger: press shift key
* Editor: press tab key

In assets subdir:
* an animation cutter (made with LÖVE)
* scripts to assemble Pencil2d output to spritesheet using montage (ImageMagick tool)

Made exclusivly on Open source tools on Arch Linux system.

Based on LÖVE game engine https://love2d.org in Lua language https://lua.org
Animation made with Pencil2D (standard branch and MyPaint branch) https://pencil2d.org/
Painting made with https://mypaint.org

Sources repository
https://framagit.org/popolon/reforest


Released unfinished for Linux Game Jam 2021

Linux and RISC-V for all!!!

* Code: GPLv3 license + Mulan License
* Gfx: CC BY 4.0

Copyleft Popolon 2021.
