--require "pop_math"

-- meshtexture animée

-- cartesian vector
meshani={
--__type="meshani",
__call=function(o,struct)
  local o={}  setmetatable(o,meshani)
  o.classname="meshani"
  o.imgname = struct.name
  o.image = lg.newImage("assets/"..o.imgname)
  o.iW,o.iH = o.image:getWidth(),o.image:getHeight()
  o.acell={w=struct.acell.w, h=struct.acell.h}
  o.adir = struct.adir
  if struct.ctr == nil then -- choisir mode centré W/2,H/2 ou centré bas W/2,H ?
    o.ctr={x=o.acell.w/2,y=o.acell.h}
  else
    o.ctr={x=struct.ctr.x,y=struct.ctr.y}
  end
  if struct.scale == nil then
    o.scale = {x=1, y=1}
  else
    o.scale = {x=struct.scale.x, y=struct.scale.y}
  end
  o.acellcoo={}
  if o.adir == "H" then
    o.frames = floor(o.iW/o.acell.w)
    if debug then print("nbr de frames="..o.frames)end
    o.fpW = o.acell.w/o.iW
    o.fpH = 1
    local xc=0
    for i=1,o.frames do
      o.acellcoo[i]={x=xc,y=0}
      xc = xc+o.fpW
    end
  elseif o.adir == "V" then
    o.frames = floor(o.iH/o.acell.h)
    if debug then print("nbr de frames="..o.frames)end
    o.fpW = 1
    o.fpH = o.acell.h/o.iH
    local yc=0
    for i=1,o.frames do
      o.acellcoo[i]={x=0,y=yc}
      yc = yc+o.fpH
    end
  end
  o.ftable = {}
  for i=1,#struct.ftable do
    o.ftable[i] = struct.ftable[i]
  end
--  if struct.meshi ~= nil then
  o:mesh_create(struct.meshi)
--  end
  struct.inst = o
  return o
end,

mesh_create=function(o,meshi)
  o.vct={}
  o.meshi={dir=meshi.dir,steps=meshi.steps}
  local vct = o.vct
  local frames = o.frames
  local steps = o.meshi.steps
  local step,pstep = 0,0
  local cs,cps = 0,0
  if o.meshi.dir == "V" then
    step,pstep = o.acell.h/steps, o.fpH/steps
    o.meshi.step,o.meshi.pstep=step,pstep
    for i =1,steps+1 do
      table.insert(vct,
        {0, cs,  0, cps,
        1,1,1})
      table.insert(vct,
        {o.acell.w, cs,  o.fpW, cps,
        1,1,1})
	  cs,cps = cs+step,cps+pstep
    end
  elseif o.meshi.dir == "H" then
    step,pstep = o.acell.w/steps, o.fpW/steps
    o.meshi.step,o.meshi.pstep=step,pstep
    for i =1,steps+1 do
      table.insert(vct,
        {cs,0,  cps,0,
        1,1,1}) 
      table.insert(vct,
        {cs,o.acell.h,  cps,o.fpH,
        1,1,1})
      cs,cps = cs+step,cps+pstep
    end
  end
  o.meshi.step,o.meshi.pstep = step,pstep
  o.startanim = 0

  o.mesh = lg.newMesh(vct, "strip")
  o.mesh:setTexture(o.image)

  return o.meshi
end,

data_inst=function(o)
  local gfxo={}
  gfxo.vct={}
  for i=1,#o.vct do local v = o.vct[i]
    gfxo.vct[i] = {v[1], v[2], -- vector
                v[3], v[4], -- texture
                v[5], v[6], v[7]}  -- color
  end
  gfxo.mesh = lg.newMesh(gfxo.vct, "strip")
  gfxo.mesh:setTexture(o.image)
  gfxo.ctr = {x=o.ctr.x, y=o.ctr.y}
  gfxo.frm = 1
  return gfxo
end,

update_mesh=function(o, gfxo)
  gfxo.mesh:setVertices( gfxo.vct)
end,

update_anim_frame=function(o,frame,obj)
  if frame == obj.frm then return end
  obj.frm = frame
  --print('frm='..frm)
  local frames = o.frames
  local vct = obj.vct
  local steps = o.meshi.steps
  local step,pstep = o.meshi.step,o.meshi.pstep
  local px,py=o.acellcoo[obj.frm].x,o.acellcoo[obj.frm].y

  --print("frm="..frm.." px= "..px.." py="..py)
  if o.meshi.dir == "V" then
    local px2=px+o.fpW
    for i =1,steps+1 do local v1,v2=vct[i*2-1],vct[i*2]
      v1[3],v1[4]=px,py
      v2[3],v2[4]=px2,py
      py=py+pstep
    end
  elseif o.meshi.dir == "H" then
    local py2=py+o.pfH
    for i =1,steps+1 do
      v1[3],v1[4]=px,py
      v2[3],v2[4]=px,py2
      px=px+pstep
    end
  end
  obj.mesh:setVertices(vct)
end,

update_frame=function(o,frame, obj)
  local frm = o.ftable[floor(frame)]
  o:update_anim_frame(frm,obj)
end,

get_frame=function(o, obj)
  return obj.frm
end,

draw=function(o, gfxo, pos, rot, scale)
  if pos == nil then pos = {x=0,y=0} end
  if rot == nil then rot = 0 end
  if scale == nil then scale = o.scale
  else scale = {x = o.scale.x * scale.x, y = o.scale.y * scale.y} end
  lg.draw(gfxo.mesh, pos.x,pos.y, rot, scale.x,scale.y, gfxo.ctr.x,gfxo.ctr.y) -- flower
end,

draw_debug=function(o, gfxo, pos)
  if pos == nil then pos={x=0,y=0} end
  local vct = gfxo.vct
  -- print("o.ct="..#vct.." o.vct/2="..#vct/2)
  for i=1,#o.vct/2 do local V1,V2 = vct[i*2-1],vct[i*2]
--    print("i1="..(i*2-1).." vct[i*2-1][1]="..V1[1].." vct[i*2-1][2]="..V1[2])
--    print("i2="..(i*2).." vct[i*2][1]="..V2[1].." vct[i*2][2]="..V2[2])
    lg.setColor({1,0.5*i,0.5*i,.5})
    lg.line(pos.x+V1[1]-gfxo.ctr.x, pos.y+V1[2]-gfxo.ctr.y,
            pos.x+V2[1]-gfxo.ctr.x, pos.y+V2[2]-gfxo.ctr.y)
    lg.setColor({1,1,0,.5})
    lg.circle("line",pos.x+V1[1]-gfxo.ctr.x, pos.y+V1[2]-gfxo.ctr.y,3)
    lg.circle("line",pos.x+V2[1]-gfxo.ctr.x, pos.y+V2[2]-gfxo.ctr.y,3)
  end
  lg.setColor({1,0.5,0.5,.5})
  lg.rectangle('line', pos.x-gfxo.ctr.x,pos.y-gfxo.ctr.y, o.acell.w,o.acell.h)
  lg.print(gfxo.frm,pos.x+5,pos.y+5)
end,

} setmetatable(meshani,meshani)
meshani.__index=meshani
