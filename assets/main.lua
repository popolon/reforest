lg=love.graphics li=love.image
basename="marche"

debug=false

fps=12
--align=8
align = 16
W,H=800,600
iw,ih = 0,0
mx,my=0,0
images={}
aniMw,aniMh=0,0
anibx,aniby=999999,999999
aniBx,aniBy=0,0
curb=1
bgcols={
  {0,0,0,1},
  {.5,.5,.5,1},
  {1,1,1,1},
}
frames={}
shifts={}
curbgcol=2

prevmd5sum=0
prevnum=0
files={}
realframes=0

displayoutput=true
outpic=basename.."sheet.png"
outdata=basename.."sheet.lua"
appdir = love.filesystem.getAppdataDirectory()

-- get current directory
local f = assert(io.popen("pwd", 'r'))
pwd = assert(f:read('*a'))
pwd = string.gsub(pwd, "\n$", "")
f:close()
if debug then print("pwd="..pwd) end

-- md5sum of picture file to try to autodetect duplicate pictures
function get_md5sum(name)
  local f = assert(io.popen("md5sum "..name.."| cut -d ' ' -f 1", 'r'))
  local s = assert(f:read('*a'))
  s = string.gsub(s, "\n$", "")
  f:close()
--    print("name:"..name.." md5="..s)
  return s
end

-- list of all files containing the basename
function list_files(basename)
  local f = assert(io.popen("ls "..basename.."[0-9][0-9][0-9][0-9].png", 'r'))
  local s = assert(f:read('*a'))
  for line in string.gmatch(s,"([^\n]*)\n?") do
   table.insert(files,line)
  end
  table.remove(files,#files)
--  print("#files="..#files)
  if debug then
    for i = 1, #files do
      print(i.."['"..files[i].."']")
    end
  end
  --s = string.gsub(s, "\n$", "")
  f:close()
end

-- loading a frame
function frame_load(name)
  local I=#images+1
  if debug then print("I="..I)end
  local md5sum = get_md5sum(name)
  if I>1 and md5sum == prevmd5sum then
    --print("current:"..md5sum.." I-1:"..images[I-1].md5sum)
    if debug then print(name.." duplicate de "..prevnum) end
    print(name.." duplicate de "..prevnum)
    local tmpframe = {duplicate=prevnum, image=images[prevnum]}
    table.insert(images,tmpframe)
    return
  end
  prevmd5sum = md5sum
  prevnum = I
  realframes = realframes+1
  local tmpimage=lg.newImage(name)
  local tmpimagedata=li.newImageData(name)
  local iw,ih= tmpimage:getDimensions()
  local mx,my,Mx,My=iw,ih,0,0
  local amx,amy,aMx,aMy=mx,my,Mx,My

  for y = 0, ih-1 do
    for x = 0, iw-1 do
       r,g,b,a = tmpimagedata:getPixel(x,y)
       if a ~= 0 then
--	 print("pixel("..x..","..y.."): r="..r.." g="..g.." b="..b.." a="..a)
	 if x < mx then mx = x end
         if x - x%align < amx then amx = x - x%align end
	 if y < my then my = y end
         if y - y%align < amy then amy = y -  y%align end
	 if x > Mx then Mx = x end
         if x - (x%align)+align > aMx then aMx = x - (x%align)+align end
	 if y > My then My = y end
	 if y - (y%align)+align > aMy then aMy = y - (y%align)+align end
       end
    end
  end
  local tmpelt={ duplicate=false, img=tmpimage, imgdata=tmpimagedata, name=name, md5sum = md5sum, iw=iw, ih=ih,
    mx=mx, my=my, Mx=Mx, My=My,
    amx=amx, amy=amy, aMx=aMx, aMy=aMy}
  table.insert(images,tmpelt)
  if aMx-amx > aniMw then aniMw = aMx - amx end
  if aMy-amy > aniMh then aniMh = aMy - amy end
  if amx < anibx then anibx=amx end
  if amy < aniby then aniby=amy end
  if aMx > aniBx then aniBx=aMx end
  if aMy > aniBy then aniBy=aMy end
  return I
end

-- creation of output spritesheet
function spritesheet_create()
  canvas = lg.newCanvas( aniMw*realframes, aniMh)
  local quad
  lg.setCanvas(canvas)
  local Im=1
  for i = 1,realframes do
    while images[Im].duplicate ~= false do
      table.insert(frames,i-1)
      table.insert(shifts,{x=images[Im].image.amx, y=aniby})
      Im=Im+1
    end
    if debug then print("draw: i="..i.." Im="..Im) end
    local I = images[Im]
    quad=lg.newQuad(I.amx,aniby, I.aMx-I.amx, aniBy-aniby,I.img)
    lg.draw(I.img,quad,(i-1)*aniMw,0)
    table.insert(frames,i)
    table.insert(shifts,{x=images[Im].amx, y=aniby})
    Im=Im+1
  end
  lg.setCanvas()
  local imgdata= canvas:newImageData()
  image_save(imgdata, outpic)

  -- write frame/shifts informations
  print("save lua : "..pwd..'/'..outdata)
  local outlua = io.open(outdata, 'w')

  outlua:write(basename.." = {\n")
  outlua:write("  inst=nil,\n  name=\""..outpic.."\",\n")
  outlua:write("  ftable = {")
  for i=1, #frames-1 do 
    outlua:write(frames[i]..",")
  end
  outlua:write(frames[#frames])
  outlua:write("},\n")
  outlua:write("  acell = {w="..aniMw..",h="..aniMh.."},\n")
  outlua:write("  "..basename.."_shifts = {")
  for i=1,#shifts-1 do
    outlua:write("{x="..shifts[i].x..',y='..shifts[i].y..'},')
  end
  outlua:write("{x="..shifts[#shifts].x..',y='..shifts[#shifts].y..'},')
  outlua:write("}\n")
  outlua:write("  adir=\"H\",\n")
  outlua:write("  type=\"quadani\",\n");
  --outlua:write("  ctr={x="..CX..",y="..Cy.. "},\n");
  outlua:write("}\n")
  outlua:close()
  --imageData = love.image.newImageData( width, height, format, rawdata )
end

function image_save(imgdata,name)
  local dest = appdir.."love/assets/"..name
  local final = pwd..'/'..name
  print("save spritesheet : "..dest)
  filedata = imgdata:encode( "png", name )
  print("move to "..final)
  f=io.popen("mv "..dest.." "..final)
  f:close()
--  io.
  --  love.filesystem.write(filedata,"camarchepaschit.png")

  --  #local f = fopen("outpout.pic",'w')
  --  #fclose(f)
end

-- draw current frame plus informations
function frame_draw(img)
  -- print("dup:"..tostring(img.duplicate))
  if img.duplicate ~= false then img = img.image end
  lg.setColor(bgcols[curbgcol])
--  lg.rectangle("fill",0,0,img.iw,img.ih)
  lg.rectangle("fill",anibx,aniby,  aniBx-anibx,aniBy-aniby)
  lg.setColor(1,1,1,1)
  lg.draw(img.img,0,0)

--  lg.setColor(1,1,1,0.5)
--  lg.rectangle("fill",0,0,img.iw,img.ih)

  lg.setColor(1,0,0,1)
  lg.rectangle('line',img.mx,img.my, img.Mx-img.mx,img.My-img.my)
  lg.setColor(0,1,1,1)
  lg.rectangle('line',img.amx,img.amy, img.aMx-img.amx,img.aMy-img.amy)
  lg.setColor(0,0,0,.6)
  lg.rectangle("fill",0,0,500,50)
  lg.setColor(1,1,1,1)
  lg.print("name="..img.name,0,12)
  lg.print("mx="..img.mx.." my="..img.my.." Mx="..img.Mx.." My="..img.My,0,24)
  lg.print("amx="..img.amx.." amy="..img.amy.." aMx="..img.aMx.." aMy="..img.aMy.." w="..img.aMx-img.amx.." h="..img.aMy-img.amy,0,36)
end

-- init of love application
function love.load()
  list_files(basename)
  for i = 1,#files do
    frame_load(files[i])
    --frame_load(basename..string.format("%04d.png",i))
  end
  print("#images="..#images)
  spritesheet_create()
end

-- detection of keys
function love.keypressed(key)
  if key == "" then
    print("change bord")
  elseif key == 'tab' then -- move centre
    curb = curb%5+1
  elseif key == 'c' then 
    curbgcol= curbgcol % #bgcols+1
  elseif key == 'd' then
    displayoutput = not displayoutput
  end
end

-- drawing
time=love.timer.getTime()
function love.draw()
  time=love.timer.getTime()
--  print("imgnum="..math.floor((time*fps)%#images) + 1)
  local img=images[math.floor((time*fps)%#images) + 1]
  frame_draw(img)
  lg.print("nbrimages="..#images.." realframes="..realframes.." basename="..basename.. " aniMw="..aniMw.." aniMh="..aniMh.." appdir="..appdir)
  lg.setColor(0,1,0,.7)
  lg.line(anibx,0,anibx,H)
  lg.line(aniBx,0,aniBx,H)
  lg.line(0,aniby,W,aniby)
  lg.line(0,aniBy,W,aniBy)

  if displayoutput then
    lg.setColor(1,1,1,.75)
    lg.draw(canvas,0,0,0,0.25,0.25)
  end
end

