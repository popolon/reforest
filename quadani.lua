--require "pop_math"
m=math sin=m.sin cos=m.cos floor=m.floor abs=m.abs
pi=m.pi pi2=pi/2 pi4=pi/4
lg=love.graphics

-- meshtexture animée

-- cartesian vector
quadani={

__call=function(o,struct)
  local o={} setmetatable(o,quadani)
  o.classname="quadani"
  o.imgname = struct.name
  o.image = lg.newImage("assets/"..o.imgname)
  o.iW,o.iH = o.image:getWidth(),o.image:getHeight()
  o.acell={w=struct.acell.w, h=struct.acell.h}
  o.ftable = struct.ftable
  o.adir = struct.adir
  if struct.ctr == nil then -- choisir mode centré W/2,H/2 ou centré bas W/2,H ?
    o.ctr={x=o.acell.w/2, y=o.acell.h}
  else
    o.ctr={x=struct.ctr.x, y=struct.ctr.y}
  end
  if struct.scale == nil then
    o.scale = {x=1,y=1}
  else
    o.scale = {x=struct.scale.x,y=struct.scale.y}
  end
  o.acellcoo={}
  if debug then print("newquadani: "..o.imgname.." type "..o.adir) end
  if o.adir == "H" then
    o.frames = floor(o.iW/o.acell.w)
    o.fpW = o.acell.w/o.iW
    o.fpH = 1
    local xc=0
    if debug then print("nbr de frames="..o.frames)end
    for i=1,o.frames do
      o.acellcoo[i]={x=xc,y=0}
-- fpW pour mesh mais fW pour quad
      if debug then print('cell['..i..']: x='..o.acellcoo[i].x..' y='..o.acellcoo[i].y) end
      xc = xc+o.acell.w
    end
  elseif o.adir == "V" then
    o.frames = floor(o.iH/o.acell.h)
    o.fpW = 1
    o.fpH = o.acell.h/o.iH
    local yc=0
    if debug then print("nbr de frames="..o.frames)end
    for i=1,o.frames do
      o.acellcoo[i]={x=0,y=yc}
--      if debug then print("i=%d,yc=%.2f",i,yc,o.acellcoo[i].y) end
      yc = yc+o.acell.h
    end
  end
  if debug then print("nombre acellcoo:"..#o.acellcoo) end
  o.quad = lg.newQuad(o.acellcoo[1].x, o.acellcoo[1].y, o.acell.w,o.acell.h, o.image:getDimensions())

  o.ftable = {}
  for i=1,#struct.ftable do
    o.ftable[i] = struct.ftable[i]
  end

  struct.inst = o
  return o
end,

data_inst=function(o)
  local gfxo={}
  gfxo.quad = lg.newQuad(o.acellcoo[1].x, o.acellcoo[1].y, o.acell.w,o.acell.h, o.image:getDimensions())
  gfxo.ctr = {x=o.ctr.x, y=o.ctr.y}
  gfxo.frm = 1
  return gfxo
end,

draw=function(o, gfxo, pos, rot, scale, shear)
--  if debug then print("quadani draw:"..o.imgname) end
  if pos == nil then pos={x=0,y=0} end
  if rot == nil then rot=0 end
  if scale == nil then scale = o.scale
  else scale = {x= o.scale.x * scale.x, y= o.scale.y * scale.y} end
  if shear == nil then 
    lg.draw(o.image, gfxo.quad, pos.x,pos.y, rot, scale.x,scale.y, gfxo.ctr.x,gfxo.ctr.y)
  else
    lg.draw(o.image, gfxo.quad, pos.x,pos.y, rot, scale.x,scale.y, gfxo.ctr.x,gfxo.ctr.y, shear.x, shear.y)
  end
end,

draw_debug=function(o, gfxo, pos, rot, scale)
  if pos == nil then pos={x=0,y=0} end
  if scale == nil then scale = o.scale
  else scale = {x= o.scale.x * scale.x, y= o.scale.y * scale.y} end
  lg.setColor({1,0.5,0.5,.5})
  lg.rectangle('line', pos.x-gfxo.ctr.x, pos.y-gfxo.ctr.y, o.acell.w,o.acell.h)
end,

draw_anim=function(o, frame, pos, a, scale)
  if frame == nil then frame = 1 end
  if pos == nil then pos={x=0,y=0} end
  if a == nil then a=0 end
  if scale == nil then scale = o.scale
  else scale = {x= o.scale.x * scale.x, y= o.scale.y * scale.y} end

  o.startanim = time
end,

update_anim_frame=function(o,frame,obj)
  if frame == obj.frm then return end
  obj.frm = frame
--  print(o.imgname..":update_frame: frm="..obj.frm)
--  print("acellcoo[obj.frm] = "..tostring(o.acellcoo[obj.frm]))
--  print("acellcoo.x="..o.acellcoo[obj.frm].x.." acellcoo.y="..o.acellcoo[obj.frm].y)
  obj.quad:setViewport( o.acellcoo[obj.frm].x,o.acellcoo[obj.frm].y , o.acell.w,o.acell.h)
end,

update_frame=function(o,frame, obj)
  local frm = o.ftable[floor(frame)]
  o:update_anim_frame(frm,obj)
end,

get_frame=function(o, obj)
  return obj.frm
end,

-- __add=function(s,v)if type(v)=='number' then return V(s.x+v,s.y+v)else return V(s.x+v.x,s.y+v.y)end end,

} setmetatable(quadani,quadani)
quadani.__index=quadani


